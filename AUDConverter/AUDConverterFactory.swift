//
//  AUDConverterFactory.swift
//  AUDConverter
//
//  Created by Joey on 29/8/20.
//  Copyright © 2020 Ng Joey. All rights reserved.
//

import Foundation


/// The component that will do the network call
public protocol AUDConverterNetworkService {
    func request(_ apiRequest: URLRequest, completion: @escaping (Result<Data, Error>) ->())
}

/// The component that will load currency data
public protocol CurrencyDataLoader {
    func loadData(completion: @escaping (Result<[Currency], Error>) -> ())
}

public enum CurrencyDataLoadingMethod {
    case networkData(networkService: AUDConverterNetworkService, sourceURLString: String)
    case useDataLoader(dataLoader: CurrencyDataLoader)
}

public struct AUDConverterFactoryConfig {
    
    public init(dataLoadingMethod: CurrencyDataLoadingMethod) {
        self.dataLoadingMethod = dataLoadingMethod
    }
    
    let dataLoadingMethod: CurrencyDataLoadingMethod
}

enum AUDConverterError: Error {
    case invalidExchangeRate
    case parseError(message: String)
    case invalidSourceURLString(sourceURLString: String)
}

public class AUDConverterFactory {
        
    private let config: AUDConverterFactoryConfig
    
    private let dataLoadingMethod: CurrencyDataLoadingMethod
    
    /// We allow client to specify how the currency data is being retrieved.
    /// Client can provide the source url and the component that will be responsible making the network call or it can provide a data loader object that will return the currency data to be used.
    /// - Parameter config: configuration to be used to create this factory
    public init(config: AUDConverterFactoryConfig) {
        self.config = config
        self.dataLoadingMethod = config.dataLoadingMethod
    }
    
    private func initializeNewConverter(usingNetworkService networkService: AUDConverterNetworkService,
                                        fromSource sourceURLString: String,
                                        completion: @escaping (Result<AUDConverter, Error>) -> ()) {
        
        if let url = URL(string: sourceURLString) {
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "GET"
            
            networkService.request(urlRequest) { (result) in
                switch result {
                case .failure(let error):
                    let result: Result<AUDConverter, Error> = .failure(error)
                    completion(result)
                    
                case .success(let data):
                    do {
                        let parsedData = try AUDConverterDataParser.parse(data: data)
                        let audConverter = AUDConverter(data: parsedData)
                        
                        let result: Result<AUDConverter, Error> = .success(audConverter)
                        completion(result)
                    } catch {
                        let result: Result<AUDConverter, Error> = .failure(error)
                        completion(result)
                    }
                }
            }
        } else {
            let error = AUDConverterError.invalidSourceURLString(sourceURLString: sourceURLString)
            let result: Result<AUDConverter, Error> = .failure(error)
            completion(result)
        }
    }
    
    private func initializeNewConverter(usingDataLoader dataLoader: CurrencyDataLoader,
                                        completion: @escaping (Result<AUDConverter, Error>) -> ()) {
        dataLoader.loadData { (result) in
            switch result {
            case .failure(let error):
                let result: Result<AUDConverter, Error> = .failure(error)
                completion(result)
                
            case .success(let data):
                let audConverter = AUDConverter(data: data)
                
                let result: Result<AUDConverter, Error> = .success(audConverter)
                completion(result)
            }
        }
    }
    
    /// Use this function to initialize and get a currency converter object which converts aud into target currency.
    /// - Parameter completion: call back that will be triggered once the operation is done
    public func initializeNewConverter(completion: @escaping (Result<AUDConverter, Error>) -> ()) {
        
        switch dataLoadingMethod {
        case let .networkData(networkService, sourceURLString):
            self.initializeNewConverter(usingNetworkService: networkService, fromSource: sourceURLString, completion: completion)
            
        case let .useDataLoader(dataLoader):
            self.initializeNewConverter(usingDataLoader: dataLoader, completion: completion)
        }
    }
}
