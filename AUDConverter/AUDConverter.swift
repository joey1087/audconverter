//
//  AUDConverter.swift
//  AUDConverter
//
//  Created by Joey on 30/8/20.
//

import Foundation

public struct Currency {
    public let id: String
    public let code: String
    public let name: String
    public let country: String
    
    let exchangeRate: Float? //to 1 $AUD
}

public class AUDConverter {
    
    private let data: [Currency]
    
    init(data: [Currency]) {
        self.data = data
    }
    
    /// All avaialble currencies
    public var availableCurrencies: [Currency] {
        return data
    }
    
    /// Currencies that have the exchange rate information available
    public var convertableCurrencies: [Currency] {
        return data.filter { $0.exchangeRate != nil }
    }
    
    /// This function convers provided AUD amount into the amount in the provided currency
    /// - Parameters:
    ///   - amount: amount in AUD dollars
    ///   - currency: the target currency to convert the amount into 
    public func convertAUD(amount: Float, intoCurrency currency: Currency) throws -> Float {
        guard let exchangeRate = currency.exchangeRate else {
            throw AUDConverterError.invalidExchangeRate
        }
        
        return amount * exchangeRate
    }
}
