//
//  Models.swift
//  Alamofire
//
//  Created by Joey on 30/8/20.
//

import Foundation

struct ApiResponse {
    
    enum RootKeys: String, CodingKey {
        case apiVersion
        case status
        case data
    }
    
    enum DataKeys: String, CodingKey {
        case brands = "Brands"
    }
    
    enum BrandsKeys: String, CodingKey {
        case wbc = "WBC"
    }
    
    enum WBCKeys: String, CodingKey {
        case portfolios = "Portfolios"
    }
    
    enum PortfoliosKeys: String, CodingKey {
        case fx = "FX"
    }
    
    enum FXKeys: String, CodingKey {
        case products = "Products"
    }
    
    let apiVerion: String
    let status: Int
    
    let parsedData: [Currency]
}

extension ApiResponse: Decodable {
    init(from decoder: Decoder) throws {
    
        /// api version
        let container = try decoder.container(keyedBy: RootKeys.self)
        self.apiVerion = try container.decode(String.self, forKey: .apiVersion)
        
        /// status
        self.status = try container.decode(Int.self, forKey: .status)
    
        /// nested containers
        let dataContainer = try container.nestedContainer(keyedBy: DataKeys.self, forKey: .data)
        let brandContainer = try dataContainer.nestedContainer(keyedBy: BrandsKeys.self, forKey: .brands)
        let wbcContainer = try brandContainer.nestedContainer(keyedBy: WBCKeys.self, forKey: .wbc)
        let portfoliosContainer = try wbcContainer.nestedContainer(keyedBy: PortfoliosKeys.self, forKey: .portfolios)
        let fxContainer = try portfoliosContainer.nestedContainer(keyedBy: FXKeys.self, forKey: .fx)
        
        let products = try fxContainer.decode([String: SilentThrowableDecodable<Product>].self, forKey: .products)
        
        /// currency data
        self.parsedData = products.map({ obj -> Currency? in
            
            guard let product = obj.value.value else {
                return nil
            }
            
            var exchangeRate: Float?
            if let value = Float(product.buyTT) {
                exchangeRate = value
            }
            
            return Currency(id: product.id, code: product.currencyCode, name: product.currencyName, country: product.country, exchangeRate: exchangeRate)
        }).compactMap { $0 }
    }
}

struct Product: Decodable {
    
    let id: String
    let currencyCode: String
    let currencyName: String
    let country: String
    let buyTT: String
    
    private enum CodingKeys: String, CodingKey {
        case id = "ProductId"
        case rates = "Rates"
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        
        let rates = try container.decode([String: ProductRate].self, forKey: .rates)
        
        if let first = rates.first {
            self.currencyCode = first.value.currencyCode
            self.currencyName = first.value.currencyName
            self.buyTT = first.value.buyTT
            self.country = first.value.country
        } else {
            throw AUDConverterError.parseError(message: "Missing rate data for product \(id)")
        }
    }
}

struct ProductRate: Decodable {
    let currencyCode: String
    let currencyName: String
    let country: String
    let buyTT: String
}

class AUDConverterDataParser {
    static func parse(data: Data) throws -> [Currency] {
        
        let decoder = JSONDecoder()
        let parsedResult = try decoder.decode(ApiResponse.self, from: data)
    
        return parsedResult.parsedData
    }
}
