//
//  AUDConverterTests.swift
//  AUDConverterTests
//
//  Created by Joey on 29/8/20.
//  Copyright © 2020 Ng Joey. All rights reserved.
//

import XCTest
@testable import AUDConverter

class AUDConverterTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class
    }

    func testDataLoading() {
        let mockCurrencyDataLoader = MockCurrencyDataLoader()
        let config = AUDConverterFactoryConfig(dataLoadingMethod: .useDataLoader(dataLoader: mockCurrencyDataLoader))
        let factory = AUDConverterFactory(config: config)
        
        var audConverter: AUDConverter?
        
        let expectation = self.expectation(description: "Waiting for init method")
        
        factory.initializeNewConverter { (result) in
            switch result {
            case .success(let converter):
                audConverter = converter
            default:
                audConverter = nil
            }
            
            expectation.fulfill()
        }

        waitForExpectations(timeout: 5, handler: nil)
        
        XCTAssertNotNil(audConverter)
        
        /// assert this is not nil is done as above
        guard let converter = audConverter else {
            return
        }
        
        XCTAssertEqual(converter.availableCurrencies.count, 3)
        XCTAssertEqual(converter.convertableCurrencies.count, 2)
        
        /// Test that it throws for invalid currency
        let notValidCurrency = Currency(id: "", code: "", name: "", country: "", exchangeRate: nil)
        XCTAssertThrowsError(try converter.convertAUD(amount: 1.0, intoCurrency: notValidCurrency), "Should throw error") { error in
            if case AUDConverterError.invalidExchangeRate = error {} else {
                XCTFail()
            }
        }
        
        /// Test the right conversion
        let euroCurrency = Currency(id: "125", code: "EUR", name: "EUR", country: "Euro", exchangeRate: 2.0)
        let convertedAmount = try! converter.convertAUD(amount: 1.0, intoCurrency: euroCurrency)
        XCTAssertEqual(convertedAmount, 2.0)
    }

}

class MockCurrencyDataLoader: CurrencyDataLoader {
    func loadData(completion: @escaping (Result<[Currency], Error>) -> ()) {
        let result: Result<[Currency], Error> = .success(mockData)
        completion(result)
    }
    
    let mockData = [Currency(id: "123", code: "USD", name: "USD", country: "USA", exchangeRate: 1.0),
                    Currency(id: "124", code: "VND", name: "VND", country: "Vietnam", exchangeRate: nil),
                    Currency(id: "125", code: "EUR", name: "EUR", country: "Euro", exchangeRate: 2.0)
    ]
}
